package com.example.gatoxo;

import android.content.Intent;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.TreeMap;

public class Juego extends AppCompatActivity {

    private TextView txt, txtX, txtO;

    private Button c0, c1, c2, u0, u1, u2, d0, d1, d2;

    private ArrayList<Button> Reset = new ArrayList<>();

    private String[][] Tabla = new String[3][3];

    private String[] Parametros;

    private boolean Turno, Ganadores = false;

    private int Rondas, Ganadasx, Ganadaso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        for(int i=0; i<3; i++)
            for(int j=0; j<3; j++)
                Tabla[i][j]="";

        Intent intent = getIntent();
        Parametros = intent.getStringArrayExtra(MainActivity.EXTRA_MESSAGE);

        Rondas = 0;
        Ganadasx = 0;
        Ganadaso = 0;

        if(Parametros[0].equals("x"))
            Turno = true;
        else
            Turno = false;

        Ganadores = false;

        txt = (TextView) findViewById(R.id.txtRondas);
        txt.setText(txt.getText().toString() + Rondas + " de " + Parametros[1]);
        Rondas = Integer.parseInt(Parametros[1]);
        txtX = (TextView) findViewById(R.id.txtX);
        txtO = (TextView) findViewById(R.id.txtO);

        c0 = (Button)findViewById(R.id.cero0);
        c1 = (Button)findViewById(R.id.cero1);
        c2 = (Button)findViewById(R.id.cero2);
        u0 = (Button)findViewById(R.id.uno0);
        u1 = (Button)findViewById(R.id.uno1);
        u2 = (Button)findViewById(R.id.uno2);
        d0 = (Button)findViewById(R.id.dos0);
        d1 = (Button)findViewById(R.id.dos1);
        d2 = (Button)findViewById(R.id.dos2);

    }

    public void OnP(View v){

        Button btn = (Button) v;
        Reset.add(btn);
        if(Ganadores){
            for(int i = 0; i<Reset.size(); i++){
                Reset.get(i).setBackgroundResource(R.drawable.sketch);
            }
            for(int i=0; i<3; i++) {
                for (int j = 0; j < 3; j++) {
                    Tabla[i][j]="";
                }
            }
            Ganadores = false;
        }
        if((Ganadasx+Ganadaso)<Rondas){
            String Posicion = btn.getText().toString();

            int x,y;
            boolean Lleno = true;
            boolean Ganador = false;
            x = Integer.parseInt(Character.toString(Posicion.charAt(0)));
            y = Integer.parseInt(Character.toString(Posicion.charAt(1)));


            for(int i=0; i<3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (Tabla[i][j].equals("")) {
                        Lleno = false;
                        break;
                    } else
                        Lleno = true;
                }
            }

            if(Turno && Tabla[x][y].equals("")){
                btn.setBackgroundResource(R.drawable.mal);
                Tabla[x][y] = "x";
                Turno = !Turno;

                if(Tabla[0][0].equals("x") && Tabla[1][1].equals("x") && Tabla[2][2].equals("x")) {
                    c0.setBackgroundResource(R.drawable.malx);
                    u1.setBackgroundResource(R.drawable.malx);
                    d2.setBackgroundResource(R.drawable.malx);
                    Ganador = true;
                }else if(Tabla[0][2].equals("x") && Tabla[1][1].equals("x") && Tabla[2][0].equals("x")) {
                    c2.setBackgroundResource(R.drawable.malx);
                    u1.setBackgroundResource(R.drawable.malx);
                    d0.setBackgroundResource(R.drawable.malx);
                    Ganador = true;
                }else {
                    for (int i = 0; i < 3; i++) {
                        if(Tabla[i][0].equals("x") && Tabla[i][1].equals("x") && Tabla[i][2].equals("x")) {
                            Ganador = true;
                            switch (i){
                                case 0:
                                    c0.setBackgroundResource(R.drawable.malx);
                                    c1.setBackgroundResource(R.drawable.malx);
                                    c2.setBackgroundResource(R.drawable.malx);
                                    break;
                                case 1:
                                    u0.setBackgroundResource(R.drawable.malx);
                                    u1.setBackgroundResource(R.drawable.malx);
                                    u2.setBackgroundResource(R.drawable.malx);
                                    break;
                                case 2:
                                    d0.setBackgroundResource(R.drawable.malx);
                                    d1.setBackgroundResource(R.drawable.malx);
                                    d2.setBackgroundResource(R.drawable.malx);
                                    break;
                            }
                            break;
                        }
                        else if(Tabla[0][i].equals("x") && Tabla[1][i].equals("x") && Tabla[2][i].equals("x")) {
                            switch (i){
                                case 0:
                                    c0.setBackgroundResource(R.drawable.malx);
                                    u0.setBackgroundResource(R.drawable.malx);
                                    d0.setBackgroundResource(R.drawable.malx);
                                    break;
                                case 1:
                                    c1.setBackgroundResource(R.drawable.malx);
                                    u1.setBackgroundResource(R.drawable.malx);
                                    d1.setBackgroundResource(R.drawable.malx);
                                    break;
                                case 2:
                                    c2.setBackgroundResource(R.drawable.malx);
                                    u2.setBackgroundResource(R.drawable.malx);
                                    d2.setBackgroundResource(R.drawable.malx);
                                    break;
                            }
                            Ganador = true;
                            break;
                        }
                    }
                }

            }else if(!Turno && Tabla[x][y].equals("")){
                btn.setBackgroundResource(R.drawable.rec);
                Tabla[x][y] = "o";
                Turno = !Turno;

                if(Tabla[0][0].equals("o") && Tabla[1][1].equals("o") && Tabla[2][2].equals("o")) {
                    c0.setBackgroundResource(R.drawable.recfin);
                    u1.setBackgroundResource(R.drawable.recfin);
                    d2.setBackgroundResource(R.drawable.recfin);
                    Ganador = true;
                }else if(Tabla[0][2].equals("o") && Tabla[1][1].equals("o") && Tabla[2][0].equals("o")) {
                    c2.setBackgroundResource(R.drawable.recfin);
                    u1.setBackgroundResource(R.drawable.recfin);
                    d0.setBackgroundResource(R.drawable.recfin);
                    Ganador = true;
                }else {
                    for (int i = 0; i < 3; i++) {
                        if(Tabla[i][0].equals("o") && Tabla[i][1].equals("o") && Tabla[i][2].equals("o")) {
                            switch (i){
                                case 0:
                                    c0.setBackgroundResource(R.drawable.recfin);
                                    c1.setBackgroundResource(R.drawable.recfin);
                                    c2.setBackgroundResource(R.drawable.recfin);
                                    break;
                                case 1:
                                    u0.setBackgroundResource(R.drawable.recfin);
                                    u1.setBackgroundResource(R.drawable.recfin);
                                    u2.setBackgroundResource(R.drawable.recfin);
                                    break;
                                case 2:
                                    d0.setBackgroundResource(R.drawable.recfin);
                                    d1.setBackgroundResource(R.drawable.recfin);
                                    d2.setBackgroundResource(R.drawable.recfin);
                                    break;
                            }
                            Ganador = true;
                            break;
                        }
                        else if(Tabla[0][i].equals("o") && Tabla[1][i].equals("o") && Tabla[2][i].equals("o")) {
                            switch (i){
                                case 0:
                                    c0.setBackgroundResource(R.drawable.recfin);
                                    u0.setBackgroundResource(R.drawable.recfin);
                                    d0.setBackgroundResource(R.drawable.recfin);
                                    break;
                                case 1:
                                    c1.setBackgroundResource(R.drawable.recfin);
                                    u1.setBackgroundResource(R.drawable.recfin);
                                    d1.setBackgroundResource(R.drawable.recfin);
                                    break;
                                case 2:
                                    c2.setBackgroundResource(R.drawable.recfin);
                                    u2.setBackgroundResource(R.drawable.recfin);
                                    d2.setBackgroundResource(R.drawable.recfin);
                                    break;
                            }
                            Ganador = true;
                            break;
                        }
                    }
                }
            }

            if(!Lleno && Ganador || Lleno && Ganador){
                if(Tabla[x][y].equals("x")){
                    Ganadasx++;
                    txt.setText("Rondas: " + (Ganadasx+Ganadaso) + " de " + Parametros[1]);
                    txtX.setText("P1X: " + (Ganadasx));
                }else{
                    Ganadaso++;
                    txt.setText("Rondas: " + (Ganadasx+Ganadaso) + " de " + Parametros[1]);
                    txtO.setText("P2O: " + (Ganadaso));
                }
                for(int i=0; i<3; i++) {
                    for (int j = 0; j < 3; j++) {
                        Tabla[i][j]="";
                    }
                }
                Ganadores = true;
            }else if(Lleno && !Ganador){
                for(int i = 0; i<Reset.size(); i++){
                    Reset.get(i).setBackgroundResource(R.drawable.sketch);
                }
                for(int i=0; i<3; i++) {
                    for (int j = 0; j < 3; j++) {
                        Tabla[i][j]="";
                    }
                }
                Toast.makeText(this, "Nadie Gana!!", Toast.LENGTH_SHORT).show();
            }
        }else{
            if(Ganadaso<Ganadasx)
                Toast.makeText(this, "P1X: Ganador con " + Ganadasx, Toast.LENGTH_SHORT).show();
            else if (Ganadaso == Ganadasx){
                Toast.makeText(this, "DesEmpate", Toast.LENGTH_SHORT).show();
                Rondas++;
                txt.setText("Ronda Extra");
            }else
                Toast.makeText(this, "P2O: Ganador con " + Ganadaso, Toast.LENGTH_SHORT).show();
        }
    }
}
