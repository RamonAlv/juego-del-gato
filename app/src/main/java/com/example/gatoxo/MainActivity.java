package com.example.gatoxo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.gatoxo.MESSAGE";

    private EditText EtxtR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EtxtR = (EditText) findViewById(R.id.EtxtR);
    }

    public void OnSeleccion(View v){

        if(!EtxtR.getText().toString().equals("")) {

            Button btn = (Button) v;
            String[] Seleccion = new String[2];
            Seleccion[0] = btn.getText().toString();
            Seleccion[1] = EtxtR.getText().toString();

            Intent Pag2 = new Intent(this, Juego.class);

            Pag2.putExtra(EXTRA_MESSAGE, Seleccion);

            Toast.makeText(this, "Figura Seleccionada '" + Seleccion[0] + "'\n Rondas a jugar " + Seleccion[1], Toast.LENGTH_SHORT).show();

            startActivity(Pag2);
        }else
            Toast.makeText(this, "Indique cuantas Rondas jugara", Toast.LENGTH_SHORT).show();
    }
}
